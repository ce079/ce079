/*Demonstrate reading a two-dimensional array marks which stores marks of 5 students in 3 
subjects and display the highest marks in each subject.*/

#include <stdio.h>

int main()
{
  int marks[5][3],i,j;
  int max_marks;
  
  for(i=0;i<5;i++)
      {
          printf("enter the marks of the student %d \n",i+1);
          {
              for(j=0;j<3;j++)
                  {
                      printf("enter marks of the subject with sub_code [%d][%d] \n",i,j);
                      scanf("%d",&marks[i][j]);
                  }
          }
      }
      
  printf("\nThe marks of 5 students in 3 given subjects is\n");
  for(i=0;i<5;i++)
   {
     for(j=0;j<3;j++)
     {
     printf("%d ",marks[i][j]);
     }
     printf("\n");
   }
      
      for(j=0;j<3;j++)
         {
             for(i=1;i<5;i++)
                {
                    if(marks[i][j]>max_marks)
                       {
                           max_marks=marks[i][j];
                          
                       }
                 }
            printf("the highest marks in subject %d is %d \n",j,max_marks);
         }
  return 0;
}

