//transpose of a given matrix

#include <stdio.h>

int main()
{
    int i,j,rows,cols,a[10][10],b[10][10];
    
    printf("enter the number of rows and columns of the matrix\n");
    scanf("%d%d",&rows,&cols);
    
    printf("enter the array elements \n");
    
    for(i=0;i<rows;i++)
    {
        for(j=0;j<cols;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    
    printf("the matrix is : \n");
    for(i=0;i<rows;i++)
    {
        for(j=0;j<cols;j++)
        {
            printf("%d",a[i][j]);
        }
      printf("\n");
    }
    
    for(i=0;i<rows;i++)
    {
        for(j=0;j<cols;j++)
        {
            b[j][i]=a[i][j];
        }
    }
    
    printf("the transpose of the given matrix is : \n");
    
    for(i=0;i<cols;i++)
    {
        for(j=0;j<rows;j++)
        {
          printf("%d",b[i][j]);
        }
      printf("\n");
    }
    
    return 0;
}


