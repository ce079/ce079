//to find roots of quadratic equation

#include<stdio.h>
#include<math.h>
int main()
{
float a,b,c,d,r1,r2;
printf("enter the coefficiants of the quadratic equation\n");
scanf("%f%f%f",&a,&b,&c);
d=(b*b)-(4*a*c);
if(d==0)
{
r1=(-b/2*a);
r2=(-b/2*a);
printf("The roots are real and equal=%f",r1);
}
else if(d>0)
{
r1=(-b+sqrt(d))/(2*a);
r2=(-b-sqrt(d))/(2*a);
printf("the roots are real and distinct %f and %f\n",r1,r2);
}
else printf("the roots are imaginary");
return 0;
}

//AREA OF CIRCLE


#include<stdio.h>
#include<math.h>
float area(float);
int main()
{
float r,a;
printf("enter the radius of the circle\n");
scanf("%f",&r);
a=area(r);
printf("the area of the circle is %f",a);
}
float area(float r)
{
return(3.14*r*r);
}


