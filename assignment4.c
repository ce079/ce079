#include<stdio.h>
#include<math.h>
int main()
{
   float a,b,c,dis,r1,r2;
   int sign;
   printf("enter the coefficiants of the quadratic equation\n");
   scanf("%f%f%f",&a,&b,&c);
   dis=(b*b)-(4*a*c);
   
   if (dis==0)
       sign=0;
   else if(dis>0)
       sign=1;
   else sign=-1;
  
   
  switch(sign)
	    {
				case 0 : r1=(-b/(2*a));
						 r2=(-b/(2*a));
						 printf("the roots are real and equal %f\n",r1);
						 break;

				case 1 :r1=(-b+sqrt(dis))/(2*a);
						r2=(-b-sqrt(dis))/(2*a);
						printf("the roots are real and distinct %f and %f\n",r1,r2);
						break;
                        
                case -1 : printf("the roots are imaginary\n");
                          break;

				default : printf("enter valid coefficiants\n");
						  break;
                        

		 }
    return 0;
   }