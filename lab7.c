//to find average of n numbers using arrays

#include <stdio.h>

int main()
{  
   int n,i,j,sum=0,avg;

   printf("enter the number of elements in the array \n");
   scanf("%d",&n);
   
   int arr[n];
   
   for(i=0;i<n;i++)
       { 
            printf("enter the %dth array element\n",i);
            scanf("%d",&arr[i]);
       }
     
   for(i=0;i<n;i++)
       {
           sum=sum+arr[i];
       }
    
    avg=sum/n;
    
    printf("The average of the numbers is %d",avg);

    return 0;
   
}
