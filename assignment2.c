//swapping using temporary variable

#include<stdio.h>
int main()
{
	int n1,n2,temp;
	printf("enter 2 numbers \n");
	scanf("%d%d",&n1,&n2);
	printf("before swapping n1=%d and n2=%d \n",n1,n2);
	temp=n1;
	n1=n2;
	n2=temp;
	printf("after swapping n1=%d and n2=%d",n1,n2);
    return 0;
}

//swapping without temporary variable

#include<stdio.h>
int main()
{
	int n1,n2;
	printf("enter 2 numbers \n");
	scanf("%d%d",&n1,&n2);
	printf("before scanning n1=%d and n2=%d \n",n1,n2);
	n1=n1+n2;
	n2=n1-n2;
	n1=n1-n2;
	printf("after swapping n1=%d and n2=%d",n1,n2);
	return 0;
			}




