//Illustrate pointers in swapping two numbers.

#include <stdio.h>

void swap(int *x,int *y)
{
  int s;
  s=*x;
  *x=*y;
  *y=s;
}

int main()
{
    int n1,n2;
    printf("enter the first number :  ");
    scanf("%d \n",&n1);
    
    printf("enter the second number :  ");
    scanf("%d \n",&n2);
    
    printf("before swapping first number is %d and second number is %d \n",n1,n2);
    swap(&n1,&n2);
    printf("after swapping first number is %d and second number is %d \n",n1,n2);
    
    return 0;
}




//write your code here