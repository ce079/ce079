//to perform arithmetic operations using pointers

#include <stdio.h>

void add(int *x,int *y,int *t)
    {
      int s;
      *t=(*x)+(*y);
    }

void subtract(int *x,int *y,int *t)
    {
        *t=(*x)-(*y);
    }

void multiply(int *x,int *y,int *t)
    {
        *t=(*x)*(*y);
    }

void division(int *x,int *y,int *t)
    {
        *t=(*x)/(*y);
    }

void remainder(int *x, int *y, int *t)
    {
        *t=(*x)%(*y);
    }

int main()
    {
        int n1,n2,total,sub,mul,divi,rem;
        
        printf("Enter the first number : \n");
        scanf("%d",&n1);
        
        printf("Enter the second number : \n");
        scanf("%d",&n2);
        
        printf("THE FOLLOWING ARITHMETIC OPERATIONS PERFORMED - \n\n");
        
        printf("The addition of the two numbers is : \n");
        add(&n1,&n2,&total);
        printf("%d + %d = %d ",n1,n2,total);
        
        printf("\nThe subtraction of the two numbers is : \n");
        subtract(&n1,&n2,&sub);
        printf("%d - %d = %d ",n1,n2,sub);
        
        printf("\nThe multiplication of the two number is : \n");
        multiply(&n1,&n2,&mul);
        printf("%d * %d = %d",n1,n2,mul);
        
        printf("\nThe dividion of the two number is : \n");
        division(&n1,&n2,&divi);
        printf("%d / %d = %d",n1,n2,divi);
        
        printf("\nThe reminder of the two numbers is : \n");
        remainder(&n1,&n2,&rem);
        printf("(%d) % (%d) = %d",n1,n2,rem);
        
         return 0;
        
    }


//write your code here