//linearsearch

#include <stdio.h>                                 

int main()
{
    int array[100],search,n,c;
    
    printf("enter the number of elements in the array\n");
    scanf("%d",&n);
    
    printf("enter the elements of the array\n");
    for(c=0;c<n;c++)
    scanf("%d",&array[c]);
    
    printf("enter the number to be searched\n");
    scanf("%d",&search);
    
    for(c=0;c<n;c++)
    {
        if(array[c]==search)
        {
            printf("%d is at the location of %d\n",search,n);
            break;
        }
    }
    if(c==n)
    printf("%d is not present in the array\n",search);
    
    return 0;
}

//binary search

#include <stdio.h>

int main()
{
    int a[10],n,m,s,beg,end,loc;
    
    printf("enter the number of elments in the array\n");
    scanf("%d",&n);
    
    printf("enter the array elements\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    
    beg=0;
    end=n-1;
    
    printf("enter the element to be searched\n");
    scanf("%d",&s);
    
    while(beg<=end)
    {
        m=(beg+end)/2;
        
        if (a[m]==s)
        {
            loc=m+1;
            printf("the element is at the location %d",loc);
            beg=end+1;
        }
        
        else if(s>a[m])
        {
            beg=m+1;
        }
        else
        {
            end=end-1;
        }
        
    }
    
    return 0;
}
