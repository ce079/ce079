//To Develop a C program to read and print employee details using structures.

#include <stdio.h>

struct employee
  {
    char  name[30];
    int   empId;
    float salary;
  };
 
int main()
{
    struct employee emp;
     
    printf("\nEnter details of the employee :\n");
    printf("Employeee Name :");        
    gets(emp.name);
    printf("Employee ID :");          
    scanf("%d",&emp.empId);
    printf("Employee Salary :");     
    scanf("%f",&emp.salary);
     
    
    printf("\nThe details of the employee are : \n");
    printf("Employee Name: %s \n" ,emp.name);
    printf("Employee Id: %d  \n" ,emp.empId);
    printf("Employee Salary: %f \n",emp.salary);
    return 0;
}


//write your code here