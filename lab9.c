//to check if given string is a palindrome
#include <stdio.h>
#include<string.h>
int main()
{
    char str[100];
    int i, length;
    int flag = 0;
    
    printf("enter a string\n");
    scanf("%s",str);
    
    length = strlen(str);
    
    for(i=0;i < length ;i++)
    {
        if(str[i] != str[length-i-1])
            {
                flag=0;
            }
        else
            {
                flag=1;
            }
    }
    
    if(flag==0)
    printf("it is not a palindrome");
    if(flag==1)
    printf("it is a palindrome");
    
   return 0;
}

